Homecloud aims to be a place for your *private* data, accessible via various clients over the internet.

It supports standard web protocols for accessing your data:

- WebDAV for files and images/videos
- CalDAV for calendar events, tasks and notes
- CardDAV for contacts

This project is still in development, and only parts of the planned features are implemented already.

# How to run

## Start server

### Quickstart for testing (using SQLite)

```bash
docker run -it --rm -p 8000:8000 --env JWT_SECRET="some-secret" registry.gitlab.com/neonews-homecloud/api:v0.5.1
```

### With CockroachDB

```bash
docker run -it --rm -p 26257:26257 -p 8080:8080 cockroachdb/cockroach start-single-node --insecure
usql "postgresql://root@127.0.0.1:26257/?sslmode=disable"
  => CREATE DATABASE homecloud;
docker run -it --rm -p 8000:8000 --env JWT_SECRET="some-secret" --env DATABASE_URL="jdbc:postgresql://172.17.0.1:26257/homecloud?sslmode=disable" --env DATABASE_DRIVER="org.postgresql.Driver" --env DATABASE_USERNAME="root" registry.gitlab.com/neonews-homecloud/api:v0.5.1
```

On Mac, you might need to change `172.17.0.1` to `docker.host.internal`.

## Register first admin user

Browse to `http://localhost:8000` and you should be presented the first-time installation wizard.

## Use WebDAV

Use your favorite WebDAV client and connect to the server.

```bash
cadaver "http://localhost:8000/files"
```

# For developers

## Requirements

- Java
- Kotlin
- Maven

## Build

```bash
mvn clean install
```

## Run

### Migrations

```bash
java -cp ./target/homecloud-5bfdb5c-dirty.jar de.neonew.homecloud.migrations.DatabaseMigrationKt
```

Note: The Filename might differ, it is based on the latest git commit hash.

### Web server

```bash
java -jar ./target/homecloud-5bfdb5c-dirty.jar
```

### Variables

You can set these environment variables for configuration

| Variable          | Value in Dockerfile               |
| ----------------- | --------------------------------- |
| SERVER_PORT       | 8000                              |
| DIRECTORY         | /app/data                         |
| DATABASE_URL      | jdbc:sqlite:/app/database.sqlite3 |
| DATABASE_DRIVER   | org.sqlite.JDBC                   |
| DATABASE_USERNAME | &lt;not set&gt;                   |
| DATABASE_PASSWORD | &lt;not set&gt;                   |
| JWT_SECRET        | &lt;not set&gt;                   |

## Test

### Unit tests

```bash
mvn test [-Dtest=TestClass#testMethod]
```

### Integration tests

```bash
mvn failsafe:integration-test [-Dit.test=TestClass#testMethod]
```

### Litmus

WebDAV server protocol compliance test suite: http://www.webdav.org/neon/litmus/

```bash
docker run -it --rm deepdiver/docker-litmus litmus http://172.17.0.1:8000/files/
```

## Check for updates

```bash
mvn versions:display-dependency-updates
mvn versions:display-plugin-updates
```

## Docker

```bash
docker build -t homecloud:snapshot .
docker run -it --rm -p 8000:8000 --memory=256m --env JWT_SECRET="some-secret" [--env SERVER_PORT=8000 --env DIRECTORY=/app/data] homecloud:snapshot
```
