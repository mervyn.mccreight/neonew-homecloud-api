package de.neonew.homecloud

import java.io.InputStream

fun resource(filename: String): InputStream =
  object {}::class.java.getResourceAsStream(filename)
