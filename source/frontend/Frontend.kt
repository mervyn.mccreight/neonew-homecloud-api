import de.neonew.homecloud.User
import de.neonew.homecloud.getFullTailcard
import io.ktor.application.call
import io.ktor.http.content.LocalFileContent
import io.ktor.http.content.static
import io.ktor.http.content.staticRootFolder
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.util.combineSafe
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

fun Routing.webFrontend(webFrontend: File, database: Database) {
  static {
    staticRootFolder = webFrontend
    files(File("."), File("index.html"), database)
  }
}

private fun Route.files(folder: File, default: File, database: Database) =
  get("{path...}") {
    val relativePath = call.parameters.getFullTailcard("path") ?: return@get

    relativePath.takeIf { it.isEmpty() && showInstallationWizard(database) }?.let {
      call.respondRedirect("/installation")
    } ?: run {
      val dir = this@files.staticRootFolder.combine(folder)
      val file = dir.combineSafe(relativePath).takeIf { it.isFile } ?: dir.combine(default)
      if (file.isFile) {
        call.respond(LocalFileContent(file))
      }
    }
  }

private fun showInstallationWizard(database: Database): Boolean = transaction(database) {
  User.count() == 0L
}

private fun File?.combine(file: File) = when {
  this == null -> file
  else -> resolve(file)
}
