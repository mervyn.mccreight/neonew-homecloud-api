package de.neonew.homecloud

import de.neonew.homecloud.api.UserSerializer
import io.ktor.auth.Principal
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Users : IntIdTable() {
  val name = varchar("name", length = 50)
  val hashedPassword = varchar("hashed_password", length = 200)

  init {
    index(true, name)
  }
}

@Serializable(with = UserSerializer::class)
class User(id: EntityID<Int>) : IntEntity(id), Principal {
  companion object : IntEntityClass<User>(Users)

  var name by Users.name
  var hashedPassword by Users.hashedPassword
}
