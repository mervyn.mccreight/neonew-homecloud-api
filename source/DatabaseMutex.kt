package de.neonew.homecloud

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.vendors.SQLiteDialect

suspend fun <T> writeTransaction(database: Database, mutex: Mutex, statement: Transaction.() -> T): T =
  databaseWriteLock(database, mutex) {
    transaction(database, statement)
  }

private suspend fun <T> databaseWriteLock(database: Database, mutex: Mutex, block: () -> T): T =
  when (database.dialect.name) {
    SQLiteDialect.dialectName -> mutex.withLock { block() }
    else -> block()
  }
