package de.neonew.homecloud

import com.natpryce.konfig.*
import com.natpryce.konfig.ConfigurationProperties.Companion.systemProperties
import de.neonew.homecloud.api.ValidationException
import de.neonew.homecloud.authentication.basic
import de.neonew.homecloud.authentication.jwt
import de.neonew.homecloud.logging.TimedCallLogging
import de.neonew.homecloud.thumbnails.launchThumbnailGenerationConsumer
import io.ktor.application.ApplicationStopped
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.features.AutoHeadResponse
import io.ktor.features.ConditionalHeaders
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.response.respond
import io.ktor.routing.IgnoreTrailingSlash
import io.ktor.routing.routing
import io.ktor.serialization.json
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.addShutdownHook
import io.ktor.server.engine.embeddedServer
import io.ktor.server.jetty.Jetty
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Database.Companion.connect
import java.io.File
import java.time.Clock

fun main() {
  val server = server(config(), Clock.systemUTC())
  server.addShutdownHook {
    server.stop(1000, 5000)
  }
  server.start(wait = true)
}

fun server(config: Configuration, clock: Clock): ApplicationEngine {
  val port = config[Key("server.port", intType)]
  val filesystem = Filesystem(File(config[Key("directory", stringType)]))
  val webFrontend = File(config[Key("webFrontend", stringType)])
  val jwtSecret = config[Key("jwt.secret", stringType)]
  val database = connect(config)

  // This is super ugly. :(
  // https://github.com/JetBrains/Exposed/issues/587
  val sqliteWriteMutex = Mutex()

  val thumbnailGenerators = launchThumbnailGenerationConsumer(database, sqliteWriteMutex, filesystem)

  return embeddedServer(Jetty, port) {
    install(TimedCallLogging)
    install(ContentNegotiation) { json() }
    install(AutoHeadResponse)
    install(ConditionalHeaders)
    install(Authentication) { basic(database); jwt(database, clock, jwtSecret) }
    install(StatusPages) { exception<ValidationException> { call.respond(BadRequest) } }
    install(IgnoreTrailingSlash)

    routing { routes(database, sqliteWriteMutex, filesystem, webFrontend, clock, jwtSecret) }

    environment.monitor.subscribe(ApplicationStopped) {
      runBlocking { thumbnailGenerators.forEach { it.cancelAndJoin() } }
    }
  }
}

fun connect(config: Configuration): Database =
  connect(
    url = config[Key("database.url", stringType)],
    driver = config[Key("database.driver", stringType)],
    user = config.getOrElse(Key("database.username", stringType)) { "" },
    password = config.getOrElse(Key("database.password", stringType)) { "" }
  )

fun config(): Configuration = EnvironmentVariables() overriding systemProperties()
