package de.neonew.homecloud.api

import de.neonew.homecloud.Filesystem
import de.neonew.homecloud.User
import de.neonew.homecloud.authentication.PasswordHasher.hash
import de.neonew.homecloud.writeTransaction
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext
import kotlinx.coroutines.sync.Mutex
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Clock
import kotlin.text.RegexOption.IGNORE_CASE

suspend fun PipelineContext<Unit, ApplicationCall>.signUp(
  filesystem: Filesystem,
  database: Database,
  clock: Clock,
  mutex: Mutex,
  secret: String
) = when (transaction(database) { User.all().empty() }) {
  false -> call.respond(Forbidden, "Admin user already exists.")
  true -> {
    val payload = call.receive<SignUpPayload>()
    val user = writeTransaction(database, mutex) {
      val user = User.new {
        name = payload.name
        hashedPassword = hash(payload.password)
      }
      if (!filesystem.createUserDirectory(user)) {
        throw IllegalStateException("Couldn't create directory for user: ${payload.name}")
      }
      user
    }
    addTokenCookie(user, clock, secret)
    call.respond(Created, user)
  }
}

@Serializable
private class SignUpPayload(val name: String, val password: String) {
  init {
    if (name in setOf("token", "key")) {
      throw ValidationException()
    }
    if (!Regex("^[a-z]{3,}$", IGNORE_CASE).matches(name)) {
      throw ValidationException()
    }
  }
}
