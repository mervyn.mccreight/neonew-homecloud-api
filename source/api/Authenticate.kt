package de.neonew.homecloud.api

import de.neonew.homecloud.User
import de.neonew.homecloud.authentication.createJWT
import de.neonew.homecloud.authentication.verify
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.auth.UserPasswordCredential
import io.ktor.auth.authentication
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.HttpStatusCode.Companion.Unauthorized
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.util.date.toGMTDate
import io.ktor.util.pipeline.PipelineContext
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import java.time.Clock
import java.time.Duration.ofHours
import java.time.Instant.now

suspend fun PipelineContext<Unit, ApplicationCall>.authenticate(database: Database, clock: Clock, secret: String) {
  val credential = call.receive<AuthenticatePayload>().toCredential()
  when (val user = verify(credential, database)) {
    null -> call.respond(Unauthorized)
    else -> {
      addTokenCookie(user, clock, secret)
      call.respond(OK)
    }
  }
}

internal fun PipelineContext<Unit, ApplicationCall>.addTokenCookie(user: User, clock: Clock, secret: String) {
  val expiresAt = now(clock).plus(ofHours(2))
  call.response.cookies.append(
    name = "token",
    value = createJWT(user, expiresAt, clock, secret),
    httpOnly = true,
    path = "/",
    expires = expiresAt.toGMTDate()
  )
}

suspend fun PipelineContext<Unit, ApplicationCall>.authenticated() {
  val principal = call.authentication.principal<User>()!!
  call.respond(OK, principal.name)
}

@Serializable
private data class AuthenticatePayload(val name: String, val password: String) {
  fun toCredential() = UserPasswordCredential(name, password)
}
