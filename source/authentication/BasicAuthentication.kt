package de.neonew.homecloud.authentication

import de.neonew.homecloud.User
import de.neonew.homecloud.Users
import de.neonew.homecloud.authentication.PasswordHasher.verify
import io.ktor.auth.Authentication
import io.ktor.auth.UserPasswordCredential
import io.ktor.auth.basic
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction

fun Authentication.Configuration.basic(database: Database) =
  basic("basic") {
    validate { verify(it, database) }
  }

fun verify(credential: UserPasswordCredential, database: Database): User? =
  transaction(database) {
    User.find { Users.name eq credential.name }
      .firstOrNull { verify(it.hashedPassword, credential.password) }
  }
