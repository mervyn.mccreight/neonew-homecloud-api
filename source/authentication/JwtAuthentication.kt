package de.neonew.homecloud.authentication

import com.auth0.jwt.JWT.create
import com.auth0.jwt.JWT.require
import com.auth0.jwt.JWTVerifier.BaseVerification
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.algorithms.Algorithm.HMAC256
import de.neonew.homecloud.User
import io.ktor.auth.Authentication
import io.ktor.auth.basicAuthenticationCredentials
import io.ktor.auth.jwt.jwt
import io.ktor.auth.parseAuthorizationHeader
import io.ktor.http.auth.HttpAuthHeader
import io.ktor.http.auth.HttpAuthHeader.Single
import io.ktor.request.ApplicationRequest
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Clock
import java.time.Instant
import java.time.Instant.now
import java.util.Date.from

fun Authentication.Configuration.jwt(database: Database, clock: Clock, secret: String) =
  jwt("jwt") {
    verifier((require(secret.toJWTAlgorithm()) as BaseVerification).build(clock.toJWTClock()))
    validate {
      transaction(database) {
        User.findById(it.payload.subject.toInt())
      }
    }
    authHeader {
      it.request.getJwtFromBasicAuth()
        ?: it.request.getJwtFromCookie()
        ?: it.request.parseAuthorizationHeader()
    }
  }

fun createJWT(user: User, expiresAt: Instant, clock: Clock, secret: String): String = create()
  .withSubject(user.id.value.toString())
  .withIssuedAt(from(now(clock)))
  .withExpiresAt(from(expiresAt))
  .sign(secret.toJWTAlgorithm())

private fun String.toJWTAlgorithm(): Algorithm = HMAC256(this)

private fun ApplicationRequest.getJwtFromBasicAuth(): HttpAuthHeader? = this.basicAuthenticationCredentials()
  .takeIf { it?.name.equals("token", ignoreCase = true) }
  ?.password
  ?.toBearerAuthHeader()

private fun ApplicationRequest.getJwtFromCookie(): HttpAuthHeader? = call.request.cookies["token"]
  ?.toBearerAuthHeader()

private fun String?.toBearerAuthHeader(): HttpAuthHeader? = this?.let { Single("Bearer", it) }

private fun Clock.toJWTClock() = com.auth0.jwt.interfaces.Clock { from(now(this@toJWTClock)) }
