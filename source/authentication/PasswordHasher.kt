package de.neonew.homecloud.authentication

import com.lambdaworks.crypto.SCryptUtil


object PasswordHasher {
  fun hash(password: String): String =
    SCryptUtil.scrypt(password, 512, 8, 1)

  fun verify(hash: String, password: String) =
    SCryptUtil.check(password, hash)
}
