package de.neonew.homecloud.migrations

import de.neonew.exposed.migrations.Migration
import de.neonew.homecloud.Users
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.SchemaUtils.create

class V4_CreateThumbnailsQueue : Migration() {
  private object ThumbnailQueue : IntIdTable() {
    val user = reference("user_id", Users)
    val resource = varchar("resource", length = 255)
    val lockedBy = varchar("locked_by", length = 255).nullable()

    init {
      index(true, user, resource)
    }
  }

  override fun run() {
    create(ThumbnailQueue)
  }
}
