package de.neonew.homecloud.migrations

import com.natpryce.konfig.Key
import com.natpryce.konfig.stringType
import de.neonew.exposed.migrations.runMigrations
import de.neonew.homecloud.config
import de.neonew.homecloud.connect
import org.jetbrains.exposed.sql.Database
import java.io.File
import java.time.Clock
import java.time.Clock.systemUTC

fun main() {
  val config = config()
  val directory = File(config[Key("directory", stringType)])
  runMigrations(connect(config), directory, systemUTC())
}

fun runMigrations(database: Database, directory: File, clock: Clock) {
  runMigrations(database, listOf(
    V1_CreateUsers(),
    V2_CreateUsersDirectory(directory),
    V3_CreateThumbnailsDirectory(directory),
    V4_CreateThumbnailsQueue()
  ), clock)
}
