package de.neonew.homecloud.migrations

import de.neonew.exposed.migrations.Migration
import io.ktor.util.combineSafe
import java.io.File

class V2_CreateUsersDirectory(private val directory: File) : Migration() {
  override fun run() {
    if (!directory.combineSafe("users").mkdir()) {
      throw RuntimeException("Couldn't create users directory")
    }
  }
}
