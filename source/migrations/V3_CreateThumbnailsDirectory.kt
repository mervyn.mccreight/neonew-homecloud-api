package de.neonew.homecloud.migrations

import de.neonew.exposed.migrations.Migration
import io.ktor.util.combineSafe
import java.io.File

class V3_CreateThumbnailsDirectory(private val directory: File) : Migration() {
  override fun run() {
    if (!directory.combineSafe("thumbnails").mkdir()) {
      throw RuntimeException("Couldn't create thumbnails directory")
    }
  }
}
