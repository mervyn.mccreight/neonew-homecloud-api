package de.neonew.homecloud.migrations

import de.neonew.exposed.migrations.Migration
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.SchemaUtils.create

class V1_CreateUsers : Migration() {
  private object Users : IntIdTable() {
    val name = varchar("name", length = 50)
    val hashedPassword = varchar("hashed_password", length = 200)

    init {
      index(true, name)
    }
  }

  override fun run() {
    create(Users)
  }
}
