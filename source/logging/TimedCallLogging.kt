package de.neonew.homecloud.logging

import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.ApplicationCallPipeline.ApplicationPhase.Monitoring
import io.ktor.application.ApplicationFeature
import io.ktor.application.call
import io.ktor.features.toLogString
import io.ktor.util.AttributeKey
import mu.KotlinLogging
import kotlin.system.measureTimeMillis

object TimedCallLogging : ApplicationFeature<ApplicationCallPipeline, Any, TimedCallLogging> {
  override val key: AttributeKey<TimedCallLogging> = AttributeKey("Timed Call Logging")

  private val logger = KotlinLogging.logger {}

  override fun install(pipeline: ApplicationCallPipeline, configure: Any.() -> Unit): TimedCallLogging {
    pipeline.intercept(Monitoring) {
      val millis = measureTimeMillis { proceed() }
      logger.info { "${call.response.status() ?: "Unhandled"} ${call.request.toLogString()} in $millis ms" }
    }

    return this
  }
}
