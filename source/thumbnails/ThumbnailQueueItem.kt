package de.neonew.homecloud.thumbnails

import de.neonew.homecloud.User
import de.neonew.homecloud.Users
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object ThumbnailQueue : IntIdTable() {
  val user = reference("user_id", Users)
  val resource = varchar("resource", length = 255)
  val lockedBy = varchar("locked_by", length = 255).nullable()

  init {
    index(true, user, resource)
  }
}

class ThumbnailQueueItem(id: EntityID<Int>) : IntEntity(id) {
  companion object : IntEntityClass<ThumbnailQueueItem>(ThumbnailQueue)

  var user by User referencedOn ThumbnailQueue.user
  var resource by ThumbnailQueue.resource
  var lockedBy by ThumbnailQueue.lockedBy
}
