package de.neonew.homecloud.thumbnails

import de.neonew.homecloud.UserFilesystem
import de.neonew.homecloud.webdav.WebDAVResource
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.content.LocalFileContent
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext

suspend fun PipelineContext<Unit, ApplicationCall>.thumbnail(resource: WebDAVResource, filesystem: UserFilesystem) {
  if (filesystem.read(resource) == null) {
    return call.respond(NotFound)
  }

  val thumbnail = filesystem.thumbnail(resource).takeIf { it.exists() && it.isFile } ?: return call.respond(NotFound)
  call.respond(LocalFileContent(thumbnail, filesystem.contentType(thumbnail)))
}
