package de.neonew.homecloud.webdav

import java.net.URI

data class WebDAVResource(val resource: String) {
  override fun toString(): String = resource
}

fun String.toResource(): WebDAVResource? =
  URI(this)
    .normalize()
    .path
    ?.let { Regex("^/?files/(.+)$").matchEntire(it) }
    ?.groupValues
    ?.get(1)
    ?.let { WebDAVResource(it) }
