package de.neonew.homecloud.webdav

import org.w3c.dom.Document
import org.xml.sax.InputSource
import java.io.StringReader
import javax.xml.namespace.NamespaceContext
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathFactory

fun String.parseXmlDocument(): Document =
  createDocumentBuilder().parse(InputSource(StringReader(this)))

fun xpath(): XPath {
  val xpath = XPathFactory.newInstance().newXPath()
  xpath.namespaceContext = DavNamespaceContext()
  return xpath
}

private fun createDocumentBuilder(): DocumentBuilder =
  createDocumentBuilderFactory().newDocumentBuilder()

private fun createDocumentBuilderFactory(): DocumentBuilderFactory {
  val documentBuilderFactory = DocumentBuilderFactory.newInstance()
  documentBuilderFactory.isNamespaceAware = true
  return documentBuilderFactory
}

private class DavNamespaceContext : NamespaceContext {
  override fun getNamespaceURI(prefix: String): String =
    when (prefix) {
      "d" -> "DAV:"
      else -> throw IllegalArgumentException(prefix)
    }

  override fun getPrefix(prefix: String): String =
    throw NotImplementedError()

  override fun getPrefixes(prefix: String): Iterator<String> =
    throw NotImplementedError()
}
