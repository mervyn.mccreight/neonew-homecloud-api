package de.neonew.homecloud.webdav

import de.neonew.homecloud.UserFilesystem
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.ContentType.Application.Xml
import io.ktor.http.HttpHeaders.Depth
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.MultiStatus
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.util.date.*
import io.ktor.util.pipeline.*
import kotlinx.dom.childElements
import org.redundent.kotlin.xml.Node
import org.redundent.kotlin.xml.PrintOptions
import org.redundent.kotlin.xml.XmlVersion.V10
import org.redundent.kotlin.xml.xml
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.xml.sax.SAXParseException
import javax.xml.xpath.XPathConstants.NODE
import kotlin.Int.Companion.MAX_VALUE
import kotlin.text.Charsets.UTF_8

suspend fun PipelineContext<Unit, ApplicationCall>.browseDirectory(
  resource: WebDAVResource,
  filesystem: UserFilesystem
) {
  val document = try {
    call.receiveText().takeIf { it.isNotEmpty() }?.parseXmlDocument()
  } catch (e: SAXParseException) {
    return call.respond(BadRequest)
  }
  val requestedProps = document?.readRequestedProps() ?: allProps

  if (!filesystem.exists(resource)) {
    return call.respond(NotFound)
  }

  val depth = call.request.header(Depth)?.toIntOrNull()?.takeIf { it >= 0 } ?: MAX_VALUE
  val propname = document?.let { xpath().evaluate("/d:propfind/d:propname", it, NODE) != null } ?: false

  val response = xml(root = "d:multistatus", version = V10) {
    namespace("d", "DAV:")
    filesystem.browseDirectory(resource, depth).forEach {
      val props = props(it, filesystem, requestedProps)

      "d:response" {
        "d:href" {
          -filesystem.location(it)
        }
        when (propname) {
          true -> addProps(props.keys.associateBy({ key -> key }, { fun Node.() {} }), "HTTP/1.1 200 OK")
          false -> {
            addProps(props, "HTTP/1.1 200 OK")
            addProps(
              (requestedProps - props.keys).associateBy({ key -> key }, { fun Node.() {} }),
              "HTTP/1.1 404 Not Found"
            )
          }
        }
      }
    }
  }

  call.respondText(
    text = response.toString(PrintOptions(singleLineTextElements = true)),
    contentType = Xml.withCharset(UTF_8),
    status = MultiStatus
  )
}

private fun Node.addProps(props: Map<String, Node.() -> Unit>, status: String) {
  if (props.isNotEmpty()) {
    "d:propstat" {
      "d:prop" {
        props.forEach { prop ->
          prop.key { prop.value(this) }
        }
      }
      "d:status" {
        -status
      }
    }
  }
}

private val allProps = listOf(
  "d:getlastmodified",
  "d:getcontentlength",
  "d:resourcetype",
  "d:getcontenttype",
  "d:getetag",
  "d:quota-used-bytes",
  "d:quota-available-bytes"
)

private fun Document.readRequestedProps(): List<String>? {
  val element = xpath().evaluate("/d:propfind/d:prop", this, NODE) as Element?
  return element?.let { it.childElements().map { child -> "d:${child.localName.lowercase()}" } }
}

private fun props(
  resource: WebDAVResource,
  filesystem: UserFilesystem,
  requestedProps: List<String>
): Map<String, Node.() -> Unit> {
  val file = filesystem.file(resource)
  return when {
    file.isFile -> mapOf(
      "d:getlastmodified" to fun Node.() {
        -GMTDate(file.lastModified()).toHttpDate()
      },
      "d:getcontentlength" to fun Node.() {
        -"${file.length()}"
      },
      "d:resourcetype" to fun Node.() {},
      "d:getcontenttype" to fun Node.() {
        -"${filesystem.contentType(resource)}"
      })
    file.isDirectory -> mapOf(
      "d:getlastmodified" to fun Node.() {
        -GMTDate(file.lastModified()).toHttpDate()
      },
      "d:resourcetype" to fun Node.() {
        "d:collection" {}
      })
    else -> mapOf()
  }.filterKeys { it in requestedProps }
}
