package de.neonew.homecloud.webdav

import de.neonew.homecloud.UserFilesystem
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpHeaders.Location
import io.ktor.http.HttpStatusCode.Companion.Conflict
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.MethodNotAllowed
import io.ktor.http.HttpStatusCode.Companion.UnsupportedMediaType
import io.ktor.request.receiveText
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext

suspend fun PipelineContext<Unit, ApplicationCall>.createDirectory(resource: WebDAVResource, filesystem: UserFilesystem) {
  if (call.receiveText().isNotEmpty()) {
    return call.respond(UnsupportedMediaType)
  }

  if (!filesystem.parentExists(resource)) {
    return call.respond(Conflict)
  }

  if (!filesystem.createDirectory(resource)) {
    return call.respond(MethodNotAllowed)
  }

  call.response.header(Location, filesystem.location(resource))
  call.respond(Created)
}
