package de.neonew.homecloud.webdav

import de.neonew.homecloud.UserFilesystem
import io.ktor.application.ApplicationCall
import io.ktor.util.pipeline.PipelineContext

suspend fun PipelineContext<Unit, ApplicationCall>.move(resource: WebDAVResource, filesystem: UserFilesystem) {
  copyOrMove(resource, filesystem) { source: WebDAVResource, destination: WebDAVResource ->
    filesystem.delete(destination)
    filesystem.move(source, destination)
  }
}
