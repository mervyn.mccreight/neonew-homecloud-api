package de.neonew.homecloud.webdav

import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Copy
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Mkcol
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Move
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Propfind
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Proppatch
import io.ktor.application.ApplicationCall
import io.ktor.routing.Route
import io.ktor.routing.method
import io.ktor.util.pipeline.ContextDsl
import io.ktor.util.pipeline.PipelineInterceptor

@ContextDsl
fun Route.propfind(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Propfind) { handle(body) }

@ContextDsl
fun Route.proppatch(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Proppatch) { handle(body) }

@ContextDsl
fun Route.mkcol(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Mkcol) { handle(body) }

@ContextDsl
fun Route.copy(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Copy) { handle(body) }

@ContextDsl
fun Route.move(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Move) { handle(body) }

