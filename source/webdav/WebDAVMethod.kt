package de.neonew.homecloud.webdav

import io.ktor.http.HttpMethod

sealed class WebDAVMethod {
  companion object {
    val Propfind = HttpMethod("PROPFIND")
    val Proppatch = HttpMethod("PROPPATCH")
    val Mkcol = HttpMethod("MKCOL")
    val Copy = HttpMethod("COPY")
    val Move = HttpMethod("MOVE")
  }
}
