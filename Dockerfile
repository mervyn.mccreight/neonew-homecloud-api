FROM bellsoft/liberica-openjdk-alpine:14

ENV APPLICATION_USER http
ENV SERVER_PORT 8000
ENV DIRECTORY /app/data
ENV WEBFRONTEND /app/web
ENV DATABASE_URL jdbc:sqlite:/app/database.sqlite3
ENV DATABASE_DRIVER org.sqlite.JDBC

RUN adduser --disabled-password --gecos '' $APPLICATION_USER

RUN mkdir -p /app/data && mkdir -p /app/web
RUN chown -R $APPLICATION_USER /app

# Workaround for sqlite problem on armv7 alpine:
# Caused by: java.lang.Exception: No native library is found for os.name=Linux-Alpine and os.arch=armv7. path=/org/sqlite/native/Linux-Alpine/armv7
#   at org.sqlite.SQLiteJDBCLoader.loadSQLiteNativeLibrary(SQLiteJDBCLoader.java:335)
# Related? https://github.com/xerial/sqlite-jdbc/issues/209
# See also: org.sqlite.util.OSInfo.isAlpine
RUN rm /etc/os-release

USER $APPLICATION_USER

COPY ./target/homecloud-*.jar /app/application.jar
COPY ./web/dist/. /app/web/
WORKDIR /app

CMD java -cp application.jar de.neonew.homecloud.migrations.DatabaseMigrationKt \
  && java -server -jar application.jar
