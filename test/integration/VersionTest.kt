package de.neonew.homecloud.tests.integration

import io.kotest.matchers.shouldBe
import io.ktor.client.call.receive
import io.ktor.client.request.accept
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.contentType
import io.ktor.http.withCharset
import kotlin.text.Charsets.UTF_8

class VersionTest : IntegrationTest({
  "/version" {
    val response: HttpResponse = client.get("/version") {
      accept(Json)
    }

    response.status shouldBe OK
    response.contentType() shouldBe Json.withCharset(UTF_8)
    response.receive<String>() shouldBe """{"commitId":"3684b5c","time":"2020-04-26T07:23:43+0200","tag":"tag"}"""
  }
})
