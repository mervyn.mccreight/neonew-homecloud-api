import io.kotest.assertions.Actual
import io.kotest.assertions.Expected
import io.kotest.assertions.intellijFormatError
import io.kotest.assertions.show.show
import io.kotest.matchers.MatcherResult
import io.kotest.matchers.neverNullMatcher

fun beEqualIgnoringWhitespaces(other: String) = neverNullMatcher<String> { value ->
  MatcherResult(
    value.removeWhitespaces() == other.removeWhitespaces(),
    { intellijFormatError(Expected(other.show()), Actual(value.show())) },
    { "${value.show().value} should not be equal ignoring whitespaces ${other.show().value}" }
  )
}

private fun String.removeWhitespaces(): String = Regex("\\s").replace(this, "")
