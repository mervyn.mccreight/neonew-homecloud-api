package de.neonew.homecloud.tests.integration.matchers

import io.kotest.matchers.Matcher
import io.kotest.matchers.MatcherResult
import java.io.File
import java.time.Instant

fun beLastModifiedAt(instant: Instant): Matcher<File> = object : Matcher<File> {
  override fun test(value: File): MatcherResult {
    val actual = value.lastModified() / 1_000
    val expected = instant.epochSecond
    return MatcherResult(
      expected == actual,
      "File should have been modified on $expected, but was $actual",
      "File should not have been modified on $actual, but was"
    )
  }
}
