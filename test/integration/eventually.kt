package de.neonew.homecloud.tests.integration

import io.kotest.assertions.timing.eventually
import kotlin.time.Duration

suspend fun <T> eventually(body: suspend () -> T): T = eventually(Duration.seconds(10), body)
