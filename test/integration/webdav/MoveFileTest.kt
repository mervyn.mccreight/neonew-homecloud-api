package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.tests.integration.matchers.beLastModifiedAt
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Move
import io.kotest.matchers.file.exist
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode.Companion.Conflict
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.NoContent
import io.ktor.http.HttpStatusCode.Companion.PreconditionFailed
import java.time.Instant.now

class MoveFileTest : IntegrationTest({
  "MOVE a file should work" {
    client.uploadFile("/example.txt")
    val before = now(clock)
    clock.advanceByHours(2)

    val response = client.move("/files/example.txt", "/files/example2.txt")

    response.status shouldBe Created
    response.headers["DAV"] shouldBe "1"
    response.headers["Location"] shouldBe "/files/example2.txt"

    userFile("example.txt") shouldNot exist()
    val file = userFile("example2.txt")
    file should beLastModifiedAt(before)
    file.readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "MOVE a file to itself" {
    client.uploadFile("/example.txt")

    val response = client.move("/files/example.txt", "/files/example.txt")

    response.status shouldBe Forbidden
    userFile("example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "MOVE to existing file" {
    client.uploadFile("/example.txt")
    client.uploadContent("overwritten", "/example2.txt")

    val response = client.move("/files/example2.txt", "/files/example.txt")

    response.status shouldBe NoContent
    response.headers["Location"] shouldBe "/files/example.txt"
    userFile("example.txt").readText() shouldBe "overwritten"
    userFile("example2.txt") shouldNot exist()
  }

  "MOVE don't overwrite file" {
    client.uploadFile("/example.txt")
    client.uploadContent("overwritten", "/example2.txt")

    val response = client.move("/files/example2.txt", "/files/example.txt") {
      header("Overwrite", "F")
    }

    response.status shouldBe PreconditionFailed
    userFile("example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "MOVE overwrite directory" {
    client.mkcol("/files/destination/")
    client.uploadFile("/example.txt", "/destination/example.txt")

    client.uploadFile("/example.txt")

    val response = client.move("/files/example.txt", "/files/destination") {
      header("Overwrite", "T")
    }

    response.status shouldBe NoContent
    response.headers["Location"] shouldBe "/files/destination"
    userFile("destination").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "MOVE a file to non-existing collection" {
    client.uploadFile("/example.txt")

    val response = client.move("/files/example.txt", "/files/non-existing/example2.txt")

    response.status shouldBe Conflict
  }
})

internal suspend fun HttpClient.move(url: String, destination: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse =
  request {
    method = Move
    url(url)
    header("Destination", destination)
    apply(block)
  }
