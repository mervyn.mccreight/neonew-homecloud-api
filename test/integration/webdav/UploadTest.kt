package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.resource
import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.tests.integration.matchers.beLastModifiedAt
import io.kotest.assertions.fail
import io.kotest.matchers.file.exist
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.client.request.put
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders.LastModified
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.content.ByteArrayContent
import io.ktor.http.toHttpDateString
import java.time.Instant
import java.time.Instant.now

class UploadTest : IntegrationTest({
  "PUT to /files should work" {
    val response = client.uploadFile("/example.txt")

    response.status shouldBe Created
    response.headers["DAV"] shouldBe "1"
    response.headers["Location"] shouldBe "/files/example.txt"
    userFile("example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "PUT to /files should have correct lastModified time" {
    client.uploadFile("/example.txt", "/example1.txt")

    val specificTimestamp = Instant.parse("2020-02-28T13:37:00Z")
    client.uploadFile("/example.txt", "/example2.txt") {
      header(LastModified, specificTimestamp.toHttpDateString())
    }

    userFile("example1.txt") should beLastModifiedAt(now(clock))
    userFile("example2.txt") should beLastModifiedAt(specificTimestamp)
  }

  "PUT to already existing file should overwrite it contents" {
    client.uploadFile("/example.txt")

    val response = client.uploadContent("New content\n", "/example.txt")

    response.status shouldBe Created
    userFile("example.txt").readText() shouldBe "New content\n"
  }

  "PUT to existing sub-directory in /files should work" {
    val directory = userFile("existing")
    if (!directory.mkdir()) fail("Couldn't create directory $directory")

    val response = client.uploadFile("/example.txt", "/existing/example.txt")

    response.status shouldBe Created
    userFile("existing/example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "PUT to non-existing sub-directory in /files should return HTTP 404" {
    val response = client.uploadFile("/example.txt", "/non-existing/example.txt")

    response.status shouldBe NotFound
    userFile("non-existing/example.txt") shouldNot exist()
  }
})

internal suspend fun HttpClient.uploadFile(filename: String, to: String = filename, contentType: ContentType? = null, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse =
  resource(filename).use {
    put("/files$to") {
      body = ByteArrayContent(it.readAllBytes(), contentType)
      apply(block)
    }
  }

internal suspend fun HttpClient.uploadContent(content: String, to: String): HttpResponse =
  put("/files$to") {
    body = content
  }
