package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.tests.integration.IntegrationTest
import io.kotest.matchers.file.exist
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.ktor.client.request.header
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.NoContent
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.HttpStatusCode.Companion.PreconditionFailed

class CopyDirectoryTest : IntegrationTest({
  "COPY a directory should work" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    val response = client.copy("/files/source/", "/files/destination/")

    response.status shouldBe Created
    response.headers["DAV"] shouldBe "1"
    response.headers["Location"] shouldBe "/files/destination/"
    userFile("destination/example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "COPY a directory with Depth: infinity" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    val response = client.copy("/files/source/", "/files/destination/") {
      header("Depth", "infinity")
    }

    response.status shouldBe Created
    response.headers["Location"] shouldBe "/files/destination/"
    userFile("destination/example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "COPY a directory with Depth: 0" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    val response = client.copy("/files/source/", "/files/destination/") {
      header("Depth", "0")
    }

    response.status shouldBe Created
    response.headers["Location"] shouldBe "/files/destination/"
    userFile("destination/example.txt") shouldNot exist()
  }

  "COPY a directory into sub-directory" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    val response = client.copy("/files/source/", "/files/source/sub-dir")

    response.status shouldBe Created
    response.headers["Location"] shouldBe "/files/source/sub-dir"
    userFile("source/sub-dir/example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
    // See https://youtrack.jetbrains.com/issue/KT-38678
    userFile("source/sub-dir/sub-dir") shouldNot exist()
  }

  "COPY a directory to existing directory" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    client.mkcol("/files/destination/")
    client.uploadFile("/example.txt", "/destination/existing.txt")

    val response = client.copy("/files/source/", "/files/destination/")

    response.status shouldBe NoContent
    response.headers["Location"] shouldBe "/files/destination/"
    userFile("destination/example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
    userFile("destination/existing.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
  }

  "COPY don't overwrite directory" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    client.mkcol("/files/destination/")
    client.uploadFile("/example.txt", "/destination/existing.txt")

    val response = client.copy("/files/source/", "/files/destination/") {
      header("Overwrite", "F")
    }

    response.status shouldBe PreconditionFailed
  }

  "COPY non-existent directory" {
    val response = client.copy("/files/source/", "/files/destination/")

    response.status shouldBe NotFound
  }
})
