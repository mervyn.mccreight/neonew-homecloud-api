package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Proppatch
import io.kotest.matchers.shouldBe
import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode.Companion.Forbidden

class ProppatchTest : IntegrationTest({
  "PROPPATCH to a file is not allowed" {
    client.uploadFile("/example.txt")

    val response: HttpResponse = client.proppatch("/files/example.txt")

    response.status shouldBe Forbidden
    response.headers["DAV"] shouldBe "1"
  }
})

internal suspend fun HttpClient.proppatch(url: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse =
  request {
    method = Proppatch
    url(url)
    apply(block)
  }
