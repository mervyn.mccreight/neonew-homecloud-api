package de.neonew.homecloud.tests.integration.thumbnails

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.tests.integration.eventually
import de.neonew.homecloud.tests.integration.matchers.beLastModifiedAt
import de.neonew.homecloud.tests.integration.webdav.copy
import de.neonew.homecloud.tests.integration.webdav.mkcol
import de.neonew.homecloud.tests.integration.webdav.move
import de.neonew.homecloud.tests.integration.webdav.uploadFile
import io.kotest.matchers.file.exist
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType.Image.JPEG
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.contentType
import java.time.Instant.now

class ThumbnailTest : IntegrationTest({
  beforeTest {
    client.mkcol("/files/images")
    client.uploadFile("/forest.jpg", "/images/forest.jpg", contentType = JPEG)

    eventually {
      val thumbnail = thumbnail("images/forest.jpg")
      thumbnail should exist()
      thumbnail should beLastModifiedAt(now(clock))
    }
  }

  "Upload an image" {
    eventually {
      val thumbnailResponse = client.get<HttpResponse>("/thumbnails/images/forest.jpg")

      thumbnailResponse.status shouldBe OK
      thumbnailResponse.contentType() shouldBe JPEG
    }
  }

  "Copy an image" {
    val before = now(clock)
    clock.advanceByHours(2)
    client.copy("/files/images/forest.jpg", "/files/images/forest_copied.jpg")

    eventually {
      val thumbnail = thumbnail("images/forest_copied.jpg")
      thumbnail should exist()
      thumbnail should beLastModifiedAt(before)

      val thumbnailResponse = client.get<HttpResponse>("/thumbnails/images/forest_copied.jpg")

      thumbnailResponse.status shouldBe OK
      thumbnailResponse.contentType() shouldBe JPEG
    }
  }

  "Copy a directory" {
    client.copy("/files/images/", "/files/images_copied/")

    eventually {
      thumbnail("images_copied/forest.jpg") should exist()

      val thumbnailResponse = client.get<HttpResponse>("/thumbnails/images_copied/forest.jpg")

      thumbnailResponse.status shouldBe OK
      thumbnailResponse.contentType() shouldBe JPEG
    }
  }

  "Move an image" {
    client.move("/files/images/forest.jpg", "/files/images/forest_moved.jpg")

    eventually {
      thumbnail("images/forest_moved.jpg") should exist()

      val thumbnailResponse = client.get<HttpResponse>("/thumbnails/images/forest_moved.jpg")

      thumbnailResponse.status shouldBe OK
      thumbnailResponse.contentType() shouldBe JPEG
    }
  }

  "Move a directory" {
    client.move("/files/images/", "/files/images_moved")

    eventually {
      thumbnail("images_moved/forest.jpg") should exist()

      val thumbnailResponse = client.get<HttpResponse>("/thumbnails/images_moved/forest.jpg")

      thumbnailResponse.status shouldBe OK
      thumbnailResponse.contentType() shouldBe JPEG
    }
  }

  "Delete an image" {
    client.delete<HttpResponse>("/files/images/forest.jpg")

    eventually { thumbnail("images/forest.jpg") shouldNot exist() }

    val thumbnailResponse = client.get<HttpResponse>("/thumbnails/images/forest.jpg")

    thumbnailResponse.status shouldBe NotFound
  }

  "Delete a directory" {
    client.delete<HttpResponse>("/files/images/")

    eventually { thumbnail("images/forest.jpg") shouldNot exist() }

    val thumbnailResponse = client.get<HttpResponse>("/thumbnails/images/forest.jpg")

    thumbnailResponse.status shouldBe NotFound
  }
})
