package de.neonew.homecloud.tests.integration

import com.natpryce.konfig.Configuration
import com.natpryce.konfig.ConfigurationProperties
import com.natpryce.konfig.EnvironmentVariables
import com.natpryce.konfig.overriding
import com.statemachinesystems.mockclock.MockClock
import de.neonew.homecloud.migrations.runMigrations
import de.neonew.homecloud.server
import io.ktor.client.HttpClient
import io.ktor.client.features.auth.Auth
import io.ktor.client.features.auth.providers.BasicAuthCredentials
import io.ktor.client.features.auth.providers.basic
import io.ktor.client.features.defaultRequest
import io.ktor.client.request.host
import io.ktor.client.request.port
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.contentType
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.jetty.JettyApplicationEngineBase
import io.ktor.util.combineSafe
import org.eclipse.jetty.server.Server
import org.jetbrains.exposed.sql.Database.Companion.connect
import java.io.Closeable
import java.io.File
import java.time.Instant.parse
import java.time.ZoneId.of
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

class IntegrationTestServer(
  name: String,
  basicAuth: Boolean,
  val directory: File,
  private val jwtSecret: String,
) : Closeable {
  val client: HttpClient
  val clock: MockClock = MockClock.at(parse("2020-01-30T09:59:59.123Z"), of("America/Montreal"))

  private val databaseUrl = "jdbc:h2:mem:$name;DB_CLOSE_DELAY=-1"
  private val databaseDriver = "org.h2.Driver"
  private val server: ApplicationEngine

  private val user = "user"
  private val password = "password"

  init {
    val database = connect(databaseUrl, databaseDriver)
    runMigrations(database, directory, clock)
    directory.setLastModified(clock.millis())

    server = startServer()
    client = createHttpClient(server.port(), basicAuth)
  }

  suspend fun registerUser() {
    val response = client.post<HttpResponse>("/api/users") {
      body = """{"name":"$user","password":"$password"}"""
      contentType(Json)
    }
    if (response.status != Created) {
      throw IllegalStateException("Couldn't register user: " + response.status)
    }
    directory.combineSafe("users/1").setLastModified(clock.millis())
  }

  private fun createHttpClient(port: Int, basicAuth: Boolean): HttpClient =
    HttpClient {
      followRedirects = false
      expectSuccess = false
      if (basicAuth) {
        install(Auth) {
          basic {
            credentials { BasicAuthCredentials(user, this@IntegrationTestServer.password) }
            sendWithoutRequest { true }
          }
        }
      }
      defaultRequest {
        this.host = "localhost"
        this.port = port
      }
    }

  private fun startServer(): ApplicationEngine =
    server(
      mapOf(
        "server.port" to "0",
        "directory" to directory.absolutePath,
        "webFrontend" to "./test/resources/web",
        "database.url" to databaseUrl,
        "database.driver" to databaseDriver,
        "database.username" to "",
        "database.password" to "",
        "jwt.secret" to jwtSecret,
      ).toConfiguration(),
      clock
    ).start()

  override fun close() {
    client.close()
    server.stop(1000, 2000)
  }

  private fun Map<String, String>.toConfiguration(): Configuration = ConfigurationProperties(this.toProperties())
}

fun ApplicationEngine.port(): Int {
  // Ugly reflection access to property: https://github.com/ktorio/ktor/issues/909
  val field = JettyApplicationEngineBase::class.memberProperties.first { it.name == "server" }
  field.isAccessible = true
  val server: Server = field.get(this as JettyApplicationEngineBase) as Server
  return server.uri.port
}
