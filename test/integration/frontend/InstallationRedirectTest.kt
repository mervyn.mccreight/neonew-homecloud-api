package de.neonew.homecloud.tests.integration.frontend

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.tests.integration.api.signUp
import io.kotest.matchers.shouldBe
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode.Companion.Found
import io.ktor.http.HttpStatusCode.Companion.OK

class InstallationRedirectTest : IntegrationTest(defaultUser = false, body = {
  "If no users are present, redirect to installation page" {
    val response = client.get<HttpResponse>("/")

    response.status shouldBe Found
    response.headers["Location"] shouldBe "/installation"
  }

  "If users are present, do not redirect to installation page" {
    client.signUp("admin", "test-password")

    val response = client.get<HttpResponse>("/")

    response.status shouldBe OK
  }
})
