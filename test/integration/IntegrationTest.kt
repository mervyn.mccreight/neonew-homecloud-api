package de.neonew.homecloud.tests.integration

import io.kotest.core.listeners.TestListener
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import io.kotest.core.test.TestResult
import io.kotest.engine.spec.tempdir
import io.ktor.client.*
import io.ktor.util.*
import java.io.File

abstract class IntegrationTest(
  body: IntegrationTest.() -> Unit = {},
  defaultUser: Boolean = true,
  basicAuth: Boolean = true,
  val jwtSecret: String = "my-super-awesome-test-secret",
) : StringSpec() {

  val client: HttpClient get() = server.client
  val directory: File get() = server.directory
  val clock get() = server.clock

  private lateinit var server: IntegrationTestServer

  init {
    listener(object : TestListener {
      override suspend fun beforeTest(testCase: TestCase) {
        val name = testCase.description.name.name.replace(Regex("[^A-Za-z0-9]"), "")
        server = IntegrationTestServer(name, basicAuth, tempdir(), jwtSecret)

        if (defaultUser) {
          server.registerUser()
        }
      }

      override suspend fun afterTest(testCase: TestCase, result: TestResult) {
        server.close()
      }
    })
    body()
  }

  fun userFile(name: String): File = File(directory, "users/1").combineSafe(name)

  fun thumbnail(name: String): File = File(directory, "thumbnails/1").combineSafe(name).combineSafe("256.jpg")
}
