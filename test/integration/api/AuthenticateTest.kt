package de.neonew.homecloud.tests.integration.api

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.impl.JWTParser
import de.neonew.homecloud.tests.integration.IntegrationTest
import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.ktor.client.features.auth.Auth
import io.ktor.client.features.auth.providers.BasicAuthCredentials
import io.ktor.client.features.auth.providers.basic
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.HttpHeaders.Authorization
import io.ktor.http.HttpHeaders.Cookie
import io.ktor.http.HttpHeaders.SetCookie
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.HttpStatusCode.Companion.Unauthorized
import io.ktor.http.HttpStatusCode.Companion.UnsupportedMediaType
import io.ktor.http.contentType
import io.ktor.http.decodeURLPart
import io.ktor.http.encodeURLPath
import java.util.Date

class AuthenticateTest : IntegrationTest(jwtSecret = "this-is-another-secret", basicAuth = false, body = {
  "/authenticate returns a JWT" {
    val response = client.post<HttpResponse>("/api/authenticate") {
      body = """{"name":"user","password":"password"}"""
      contentType(Json)
    }

    response.status shouldBe OK
    val cookie = response.getTokenCookie().shouldNotBeNull()
    val token = cookie.groupValues[1].decodeURLPart()

    // verify jwt-secret is used
    shouldNotThrow<JWTVerificationException> {
      (JWT.require(Algorithm.HMAC256(jwtSecret)) as JWTVerifier.BaseVerification)
        .build { Date.from(clock.instant()) }
        .verify(token)
    }

    // authenticate via cookie
    val authenticatedViaCookie = client.get<HttpResponse>("/api/authenticated") {
      header(Cookie, "token=${token.encodeURLPath()}")
    }

    authenticatedViaCookie.status shouldBe OK
    authenticatedViaCookie.readText() shouldBe "user"

    // authenticate via basic auth with token
    val authenticatedViaBasicAuth = client.config {
      install(Auth) {
        basic {
          credentials { BasicAuthCredentials("token", token) }
          sendWithoutRequest { true }
        }
      }
    }.get<HttpResponse>("/api/authenticated")

    authenticatedViaBasicAuth.status shouldBe OK
    authenticatedViaBasicAuth.readText() shouldBe "user"

    // authenticate via auth header
    val authenticatedViaHeader = client.get<HttpResponse>("/api/authenticated") {
      header(Authorization, "Bearer $token")
    }

    authenticatedViaHeader.status shouldBe OK
    authenticatedViaHeader.readText() shouldBe "user"
  }

  "/authenticate with wrong credentials" {
    val response = client.post<HttpResponse>("/api/authenticate") {
      body = """{"name":"user","password":"wrong-password"}"""
      contentType(Json)
    }

    response.status shouldBe Unauthorized
  }

  "/authenticate without payload" {
    val response = client.post<HttpResponse>("/api/authenticate")

    response.status shouldBe UnsupportedMediaType
  }
})

internal fun HttpResponse.getTokenCookie(): MatchResult? =
  headers[SetCookie]?.let { Regex("""token=([^\s;]*); Expires=([^;]*); Path=/; HttpOnly; \${'$'}x-enc=URI_ENCODING""").matchEntire(it) }
