package de.neonew.homecloud.tests.integration.api

import de.neonew.homecloud.tests.integration.IntegrationTest
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.nulls.beNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.contentType

class SignUpTest : IntegrationTest(defaultUser = false, body = {
  "Sign up first user" {
    val response = client.signUp("admin", "test-password")

    response.status shouldBe Created
    response.readText() shouldBe """{"id":1,"name":"admin"}"""
    response.getTokenCookie() shouldNot beNull()
  }

  "Register second user results in 403 Forbidden" {
    client.signUp("admin", "test-password")

    val response = client.signUp("second-admin", "test-password")

    response.status shouldBe Forbidden
  }

  "Sign-up validation" {
    forAll(
      row("token", "test-password"),
      row("key", "test-password"),
      row("a", "test-password"),
      row("special-characters-#+/", "test-password")
    ) { name, password ->
      val response = client.signUp(name, password)
      response.status shouldBe BadRequest
    }
  }
})

internal suspend fun HttpClient.signUp(name: String, password: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse =
  post("/api/users") {
    body = """{"name":"$name","password":"$password"}"""
    contentType(Json)
    apply(block)
  }
