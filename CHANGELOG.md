# v0.3.0
**2020-05-14**
- Frontend #81

# v0.2.0
**2020-05-13**
- Basic authentication #2
- Get users from database (SQLite as default, support for CockroachDB) #8 #76
- Database migrations #9
- First REST API endpoint: Create admin user #80
- Run integration tests in parallel #77

# v0.1.1
**2020-05-05**
- Copy/Move files and directories #42 #43 #44 #58 #61 #66 #67
- Improved PROPFIND #33 #37 #38 #39 #59 #60
- Keep Last-Modified date of uploaded files #29
- Show filesize on downloads #68
- Cache downloads #69
- /version and /status endpoint #54 #56
- Improved logging #62 #63 #64

# v0.1.0
**2020-04-26**
- First release
- Basic WebDAV functionality: Download, upload and browse files
