#!/usr/bin/env bash

# Example call: env GITLAB_TOKEN=xxx ./issues-since-last-release.sh

LATEST_TAG=`git describe --tags --abbrev=0 HEAD^`
GITLAB_PROJECT_ID="18241446"

echo "Changes from ${LATEST_TAG}..HEAD"

git log --format=format:'%s' ${LATEST_TAG}..HEAD | grep -oP '#\K\d+' \
  | sort -un \
  | GITLAB_PROJECT_ID=${GITLAB_PROJECT_ID} xargs -n1 bash -c 'curl -s -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/issues/${0}" | jq -r '\''"#\(.iid) \(.title): \(.state)"'\'''
